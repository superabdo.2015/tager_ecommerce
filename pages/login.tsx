
import styles from '@/styles/login.module.css'
import Image from 'next/image'
import Img from '../public/img/user.png'
import Link from 'next/link'
import { useState } from 'react';
import Router from 'next/router';


export default function LoginPage (){

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const res = await fetch('/api/login', {
            body: JSON.stringify({
                email: email,
                password: password
            }),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST'
        });
        if (res.ok) {
            const result = await res.json();
            document.cookie = `authToken=${result.token}; path=/;`;
            Router.push('/');
        } else {
            console.log('Login failed.');
        }
    };

    return(
        <div className={styles.main}>  
            <div className={styles.imguser}>
                 <Image  src={Img} alt="error"  width={80} height={80} />
            </div>      

            <form className={styles.login_form} onSubmit={handleSubmit}>

                <h1 className={styles.sign}>تسجيل الدخول الي حسابك</h1>
                <input className={styles.the_input} type="email" id="email" value={email} onChange={(event) => setEmail(event.target.value)} placeholder='البريد الإلكتروني'  ></input>
                <input className={styles.the_input} type="password" id="password" value={password} onChange={(event) => setPassword(event.target.value)} placeholder='كلمه المرور'></input>
                <Link href="#" className={styles.rem_password} >! نسيت كلمه المرور</Link>
                <button className={styles.btn_signup} type='submit'>تسجيل الدخول</button>
                <p className={styles.second1}>ليس لديك حساب؟ 
                    <Link className={styles.second2} href='/signup'>إنشاء حساب  </Link>
                </p>
            </form>

        </div>
    )
}





