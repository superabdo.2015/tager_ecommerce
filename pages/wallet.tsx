import * as React from 'react';
import Appbar from '../components/bars/appbar';
import Image from 'next/image'
import sa from '../public/img/profit.png'
import saving from '../public/img/saving.png'
import styles from '@/styles/wallet.module.css'
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';




const emails = ['username@gmail.com', 'user02@gmail.com'];

export interface SimpleDialogProps {
  open: boolean;
  selectedValue: string;
  onClose: (value: string) => void;
}

function SimpleDialog(props: SimpleDialogProps) {
    const { onClose, selectedValue, open } = props;
  
    const handleClose = () => {
      onClose(selectedValue);
    };


    const [age, setAge] = React.useState('');

    const handleChange = (event: SelectChangeEvent) => {
      setAge(event.target.value);
    };


  
    return (

        <Dialog onClose={handleClose} open={open}>

          <form className={styles.dialog}>
            <div className={styles.withdraw_profits}>
                <p className={styles.name2_withdraw_profits}>سحب الأرباح</p>
            </div>

            <p className={styles.name3_withdraw_profits}>المبلغ المراد سحبه</p>
            <div className={styles.withdraw_profits2}>
                <div className={styles.div_input_sumtotal}>
                    <span>SAR</span>
                    <input type='tel' className={styles.input_sumtotal} />
                </div>
            </div>

                <p className={styles.name3_withdraw_profits}>طريقه الدفع</p>
            <div className={styles.withdraw_profits2}>

                <FormControl className={styles.select_way_pay}>
                  <Select 
                  className={styles.select_way_pay}
                  value={age}
                  onChange={handleChange}
                  displayEmpty
                  inputProps={{ 'aria-label': 'Without label' }}
                  >

                    <MenuItem value={10}>
                      <p>فوادفون كاش</p>
                    </MenuItem>
                    <MenuItem value={20}>
                      <p>اورنج كاش</p>
                    </MenuItem>
                    <MenuItem value={30}>
                      <p>اتصالات كاش</p>  
                    </MenuItem>
                  </Select>
                </FormControl>


            </div>

            <div className={styles.sure_pay}>
              <Button ><p className={styles.name_sure_pay}>تأكيد السحب</p></Button>
            </div>
          </form>


        </Dialog>

    );
  }

export default function Wallet() {

    const [open, setOpen] = React.useState(false);
    const [selectedValue, setSelectedValue] = React.useState(emails[1]);
  
    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleClose = (value: string) => {
      setOpen(false);
      setSelectedValue(value);
    };






  return (
    <>
    <Appbar/>


    <div className={styles.div_walletname}>
        <p className={styles.walletname}>المحفظة الخاصه بك</p>
    </div>
    <div className={styles.divwalletname}>
        <div className={styles.divwalletname2}>
            <div className={styles.div_profits}>
                <div>
                    <p className={styles.ready_profits}>أرباح جاهزه للسحب  </p>
                    <p className={styles.value_profit}>0 ريال سعودي</p>
                </div>

                <Image src={sa} alt="error" className={styles.salogo}  />

            </div>
            <hr className={styles.line}/>
            
            <div className={styles.div_profits}>
                <div>
                    <p className={styles.ready_profits}>أرباح في الإنتظار  </p>
                    <p className={styles.value_profit}>0 ريال سعودي</p>
                </div>
                <Image  src={saving} alt="error"  className={styles.salogo} />
            </div>

            <div>
            <Typography variant="subtitle1" component="div">
                {/* Selected: {selectedValue} */}
            </Typography>
            <br />
            <Button className={styles.btn_withdraw_profits}  onClick={handleClickOpen}>
                <span className={styles.name_withdraw_profits}>سحب الأرباح</span>
            </Button>
            <SimpleDialog
                selectedValue={selectedValue}
                open={open}
                onClose={handleClose}
            />

            </div>
        </div>

    </div>



 
          
    </>
  )
}
