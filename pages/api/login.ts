import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { NextApiRequest, NextApiResponse } from 'next';
import { RowDataPacket } from 'mysql2';
import pool from '../../db';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    if (req.method === 'POST') {
        const { email, password } = req.body;
        const [rows] = await pool.query<RowDataPacket[]>('SELECT * FROM users WHERE email = ?', [email]);
        if (rows.length > 0) {
            const user = rows[0];
            const match = await bcrypt.compare(password, user.password);
            if (match) {
                const token = jwt.sign({ userId: user.id }, 'echoteamsecretkey', { expiresIn: '1d' });
                res.json({ token });
            } else {
                res.status(401).json({ message: 'Invalid password.' });
            }
        } else {
            res.status(401).json({ message: 'Invalid email.' });
        }
    } else {
        res.status(405).json({ message: 'Method not allowed.' });
    }
}
