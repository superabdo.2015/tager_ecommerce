
import Appbar from '@/components/bars/appbar';
import Slide_card_product from '@/components/slide_card_product';
import { Container } from '@mui/material';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import React from 'react';
import styles from '@/styles/product.module.css'


interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
        )}
      </div>
    );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}


export default function Products() {



  const slidesshow = [
    {
      id: 1,
      imageUrl: '/img/product2.png',
      product_title: "iphone1",
      product_price: 180,
      profit: 50,
    },
    {
      id: 2,
      imageUrl: '/img/product3.png',
      product_title: "iphone2",
      product_price: 120,
      profit: 30,
    },
    {
      id: 3,
      imageUrl: '/img/product4.png',
      product_title: "iphone3",
      product_price: 30,
      profit: 30,
    },
    {
      id: 4,
      imageUrl: '/img/product5.png',
      product_title: "iphone4",
      product_price: 180,
      profit: 50,
    },
  ];

  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };



  return ( 
    <>

    <Appbar/>

    <div className={styles.div_cards_infoproduct}>
      <div>
        <p>إسم المنتج | </p>
        <p> كود المنتج | </p>
        <p>سعر المنتج | </p>
        <p>الربح | </p>
        <p>كميه المنتج | </p>
        <hr/>
        <input type='number'></input>
        <Stack spacing={2} direction="row">
          <Button variant="contained"><p>إضافه للعربه</p></Button>
        </Stack>
        <Stack spacing={2} direction="row">
          <Button variant="contained"><p>إضافه للمفضله</p></Button>
        </Stack>
        <Stack spacing={2} direction="row">
          <Button variant="contained"><p>طلب المنتج</p></Button>
        </Stack>
      </div>

      <div className={styles.ssssssssssssssssssssssss}>
        <Slide_card_product slides={slidesshow}/>
        <div className={styles.ssssssssssssssssssssssss1}>
          <Stack sx={{ width:'74%',marginTop:'10px'}} >
            <Button sx={{backgroundColor:'#46B8B0',height:'50px','&:hover':{backgroundColor:'#46B8B0'}}} variant="contained"><p>تحميل جميع الصور</p></Button>
          </Stack>
        </div>
      </div>

    </div>

    <div>
      <Box sx={{ width: '100%' }}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
            <Tab label={<p>فيديوهات</p>} {...a11yProps(0)} />
            <Tab label={<p>تفاصيل المنتج</p>} {...a11yProps(1)} />
            <Tab label={<p>شرح المنتج</p>} {...a11yProps(2)} />
          </Tabs>
        </Box>
        <TabPanel value={value} index={0}>
        فيديوهات
        </TabPanel>
        <TabPanel value={value} index={1}>
        فيديوهات
        </TabPanel>
        <TabPanel value={value} index={2}>
        فيديوهات
        </TabPanel>
      </Box>
    </div>







    </>
  )
}
