import styles from '@/styles/signup.module.css';
import Image from 'next/image';
import Img from '../public/img/welcome-user.png';
import { useState } from 'react';
import Router from 'next/router';
import Link from 'next/link';
// import axios from 'axios';


export default function SignupPage (){

    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [confirmPassword, setConfirmPassword] = useState<string>('');

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (password !== confirmPassword) {
            console.log('Passwords do not match.');
            return;
        }
        const res = await fetch('/api/signup', {
            body: JSON.stringify({
                email: email,
                password: password
            }),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST'
        });
        if (res.ok) {
            const result = await res.json();
            document.cookie = `authToken=${result.token}; path=/;`;
            Router.push('/');
        } else {
            console.log('Signup failed.');
        }
    };
    
    return(

        <div className={styles.main}> 
            <div className={styles.img_welcome}>
                <Image src={Img} alt="error"  width={150} height={150} />
            </div> 

            <form className={styles.login_form} method='POST' onSubmit={handleSubmit}>
                
                <h1 className={styles.sign}>أنشئ حساباً و ابدأ رحلة الأرباح  </h1>

                <input className={styles.the_input}
                    placeholder='البريد اللإلكتروني' 
                    type="email" id="email" value={email} onChange={(event) => setEmail(event.target.value)}
                    />

                <input className={styles.the_input} 
                    placeholder='كلمه المرور' 
                    type="password" id="password" value={password} onChange={(event) => setPassword(event.target.value)}
                    />
                    
                <input className={styles.the_input} 
                    placeholder='تأكيد كلمه المرور' 
                    type="password" id="confirmPassword" value={confirmPassword} onChange={(event) => setConfirmPassword(event.target.value)}
                    /> 
                <input
                type='submit'
                className={styles.btn_signup}
            />
                
                <p className={styles.second1}>لدي حساب بالفعل !
                <Link className={styles.second2} href='/login'> تسجيل دخول</Link>
                </p>
            
            </form>

        </div>
    )
}



