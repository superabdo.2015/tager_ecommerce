import Appbar from '../components/bars/appbar';
import Slider from '../components/slider';
import SliderShow from '../components/slidershow';
import Catagory_index from '../components/catagory_index'


export default function Index() {
    const slides = [
        {
          id: 1,
          imageUrl: '/img/bg1.png',
          product_title: "iphone1",
          product_price: 180,
          profit: 50,
        },
        {
          id: 2,
          imageUrl: '/img/bg2.jpg',
          product_title: "iphone2",
          product_price: 120,
          profit: 30,
        },
        {
          id: 3,
          imageUrl: '/img/bg3.jpg',
          product_title: "iphone3",
          product_price: 30,
          profit: 30,
        },
        {
          id: 4,
          imageUrl: '/img/bg4.jpg',
          product_title: "iphone4",
          product_price: 180,
          profit: 50,
        },
        {
          id: 5,
          imageUrl: '/img/bg5.jpg',
          product_title: "iphone4",
          product_price: 180,
          profit: 50,
        },
      ];


    const slidesshow = [
        {
          id: 1,
          imageUrl: '/img/product2.png',
          product_title: "iphone1",
          product_price: 180,
          profit: 50,
        },
        {
          id: 2,
          imageUrl: '/img/product3.png',
          product_title: "iphone2",
          product_price: 120,
          profit: 30,
        },
        {
          id: 3,
          imageUrl: '/img/product4.png',
          product_title: "iphone3",
          product_price: 30,
          profit: 30,
        },
        {
          id: 4,
          imageUrl: '/img/product5.png',
          product_title: "iphone4",
          product_price: 180,
          profit: 50,
        },
      ];




  return (

    
    <>
      <Appbar/>

      <SliderShow slides={slides} />


      
      <Catagory_index url='/wallet' title='إلكترونيات'/>
      <Slider slides={slidesshow}/> 
      <Catagory_index url='/wallet' title='ملابس'/>
      <Slider slides={slidesshow}/> 
      <Catagory_index url='/wallet' title='منزل'/>
      <Slider slides={slidesshow}/> 
      <Catagory_index url='/wallet' title='صحه وجمال'/>
      <Slider slides={slidesshow}/> 



       

    </>

  )
}
