


import { Container } from '@mui/material';
import Appbar from '../components/bars/appbar';
import Card_fav_product from '../components/card_fav_product';
import styles from '@/styles/card_fav_product.module.css'
import Box from '@mui/material/Box';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import React from 'react';



export default function Fav() {
   const [checked, setChecked] = React.useState([true, false]);

   const handleChange1 = (event: React.ChangeEvent<HTMLInputElement>) => {
     setChecked([event.target.checked, event.target.checked]);
   };
 
   const handleChange2 = (event: React.ChangeEvent<HTMLInputElement>) => {
     setChecked([event.target.checked, checked[1]]);
   };
 
   const handleChange3 = (event: React.ChangeEvent<HTMLInputElement>) => {
     setChecked([checked[0], event.target.checked]);
   };
 
   const children = (
     <Box sx={{ display: 'flex', flexDirection: 'column', ml: 3 }}>
       <FormControlLabel
         label="Child 1"
         control={<Checkbox checked={checked[0]} onChange={handleChange2} />}
       />
       <FormControlLabel
         label="Child 2"
         control={<Checkbox checked={checked[1]} onChange={handleChange3} />}
       />
     </Box>
   );

   
  return (

    
    <>
      <Appbar/>
      <Container>
      <FormControlLabel
        label="Parent"
        control={
          <Checkbox
            checked={checked[0] && checked[1]}
            indeterminate={checked[0] !== checked[1]}
            onChange={handleChange1}
          />
        }
      />
      {children}

    </Container>



    </>

  )
}
