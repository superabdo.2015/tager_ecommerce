import React from 'react';
import { CacheProvider } from '@emotion/react';
import { CssBaseline } from '@mui/material';

import createEmotionCache from '../utility/createEmotionCache';
import '../styles/globals.css';
import { SnackbarProvider, useSnackbar } from 'notistack'
import { CartProvider } from '../src/CartContext';
import { FavProvider } from '../src/FavContext';
import { SessionProvider } from "next-auth/react"




const clientSideEmotionCache = createEmotionCache();

interface MyAppProps {
  Component: React.ElementType;
  emotionCache?: ReturnType<typeof createEmotionCache>;
  pageProps: Record<string, unknown>;
}

const MyApp: React.FC<MyAppProps> = (props) => {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  return (
    <CartProvider>
      <FavProvider>
        <SnackbarProvider maxSnack={2}>
          <CacheProvider value={emotionCache}>
              <CssBaseline />
              <Component {...pageProps} />
          </CacheProvider>
        </SnackbarProvider>
      </FavProvider>
    </CartProvider>
  );
};

export default MyApp;
