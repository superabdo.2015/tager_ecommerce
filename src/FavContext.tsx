import axios from 'axios';
import React, { createContext, useState, useEffect } from 'react';

type FavContextType = {
  FavItemsCount: number,
  addToFav: () => void,
  removeFromFav: () => void,
}

type FavProviderProps = {
  children: React.ReactNode;
}

const getTotalFavouriteITems = () => {
  return axios.get(`http://localhost:5000/users/123/getTotalFavouriteItems`)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.error(error);
      return 0;
    });
};

export const FavContext = createContext<FavContextType>({
  FavItemsCount: 0,
  addToFav: () => {},
  removeFromFav: () => {},
});

export const FavProvider: React.FC<FavProviderProps> = ({ children }) => {
  const [totalFavItems, setTotalFavItems] = useState(0);
  const [FavItemsCount, setFavItemsCount] = useState(0);

  useEffect(() => {
    getTotalFavouriteITems().then((response) => {
      setTotalFavItems(response);
    });
  }, []);

  useEffect(() => {
    setFavItemsCount(totalFavItems);
  }, [totalFavItems]);

  const addToFav = () => {
    setFavItemsCount(FavItemsCount + 1);
  };

  const removeFromFav = () => {
    setFavItemsCount(FavItemsCount - 1);
  };

  return (
    <FavContext.Provider value={{ FavItemsCount, addToFav, removeFromFav }}>
      {children}
    </FavContext.Provider>
  );
};
