import React, { createContext, useState } from 'react';

type CartContextType = {
  cartItemsCount: number,
  addToCart: () => void,
  removeFromCart: () => void,
}

type CartProviderProps = {
  children: React.ReactNode;
}

export const CartContext = createContext<CartContextType>({
  cartItemsCount: 0,
  addToCart: () => {},
  removeFromCart: () => {},
});

export const CartProvider: React.FC<CartProviderProps> = ({ children }) => {
  const [cartItemsCount, setCartItemsCount] = useState(0);

  const addToCart = () => {
    setCartItemsCount(cartItemsCount + 1);
  };

  const removeFromCart = () => {
    setCartItemsCount(cartItemsCount - 1);
  };

  return (
    <CartContext.Provider value={{ cartItemsCount, addToCart, removeFromCart }}>
      {children}
    </CartContext.Provider>
  );
};
