import { Splide, SplideSlide } from '@splidejs/react-splide';
import '@splidejs/splide/dist/css/themes/splide-default.min.css';
import Image from 'next/image';
import Container from '@mui/material/Container';

import styles from '@/styles/slidershow.module.css'


interface SliderProps { 
    slides: {
      id: number;
      imageUrl: string;
      product_title: string;
      product_price: number;
      profit: number;
    }[];
    options?: {
      perPage?: number;
      perMove?: number;
      width?: number;
      height?: number;
    };
  }

function SliderShow ({ slides, options }: SliderProps){


  const splideOptions = {
    type: 'loop',
    perPage: 1,
    perMove: 1,

    pagination: true,
    arrows: true,
  };

  return (
    <Container>
        <Splide options={splideOptions} className={styles.slideshow}>
          {slides.map((slides) => (
              <SplideSlide key={slides.id } className={styles.div_img_product}>
                
                <Image src={slides.imageUrl} alt={''} width={800} height={300} className={styles.img_slideshow}/>
                {/* src={slide.imageUrl} alt={slide.product_title} width={200} height={140} className={styles.img_product} */}
              </SplideSlide>
          ))}
        </Splide>    
    </Container>
  );
};

export default SliderShow;