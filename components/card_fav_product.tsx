


import Box from '@mui/material/Box';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import React from 'react';
import Button from '@mui/material/Button';
import styles from '@/styles/card_fav_product.module.css'
import { Container } from '@mui/material';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Image from 'next/image'
import Img from '../public/img/fav1.png'
import Link from 'next/link';
// icons

import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import { FavContext } from '../src/FavContext';
import { useContext } from 'react';




export default function CardFavProduct() {
  const { removeFromFav } = useContext(FavContext);


  const [checked, setChecked] = React.useState([true, false]);

  const handleChange1 = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked([event.target.checked, event.target.checked]);
  };

  const handleChange2 = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked([event.target.checked, checked[1]]);
  };

  const handleChange3 = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked([checked[0], event.target.checked]);
  };


  const child1 = (
    <Box sx={{ display: 'flex', flexDirection: 'column'}}>
      <FormControlLabel
        label=""
        control={<Checkbox checked={checked[0]} onChange={handleChange2} />}
        sx={{
            '& .MuiSvgIcon-root': {
            borderRadius: '50%',
            opacity: 0.8,
            color:'#29B6AB',
            fontSize:'26px',
            
            },
        }}
      />

    </Box>
  );
  const child2 = (
    <Box sx={{ display: 'flex', flexDirection: 'column'}}>
      <FormControlLabel
        label=""
        control={<Checkbox checked={checked[1]} onChange={handleChange3} />}
        sx={{
            '& .MuiSvgIcon-root': {
            borderRadius: '50%',
            opacity: 0.8,
            color:'#29B6AB',
            fontSize:'30px',
            
            },
        }}
      />

    </Box>
  );

  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));
   
  return (

    
    
    <Container>
        <div className={styles.control_total}>

            <Button variant="text"><span className={styles.delete_selection}>حذف المحدد</span></Button>

            <FormControlLabel
            control={
                <Checkbox
                checked={checked[0] && checked[0]}
                onChange={handleChange1}
                sx={{
                    '& .MuiSvgIcon-root': {
                    borderRadius: '50%',
                    opacity: 0.8,
                    color:'#29B6AB',
                    fontSize:'30px',
                    
                    },
                }}
                />
            }
            label={<p className={styles.selection_total}>إختر الكل</p>}
            labelPlacement="start"
            />
        </div>

        <Box sx={{ flexGrow: 1 ,marginTop:'20px' }}>
          <Grid container  spacing={2}  className={styles.grid2}  >

            <Grid item  xl={4} lg={4} md={4}  className={styles.grid} >
              <div className={styles.div_info_product}>
                  <Link href='#' className={styles.link_product}>سبراي تفحيم الكاوتش</Link> 

                  <Image  src={Img} alt="error"  width={85} height={85} className={styles.img_fav_product} />
                  <div >
                      {child1}
                      <IconButton sx={{margin:'0px',padding:'0px'}}>
                          <DeleteIcon sx={{fontSize:'28px',margin:'0px',padding:'0px',color:'#FF4966' }} />
                      </IconButton>
                  </div>
              </div>

              <div className={styles.div_info_product2}>
                  <p className={styles.money}>120 ريال سعودي </p>
                  <p className={styles.profit}>السعر  </p>
              </div>

              <div className={styles.div_info_product2}>
                  <p className={styles.money}>20 ريال سعودي </p>
                  <p className={styles.profit}>الربح  </p>
              </div>

              <div className={styles.addtoarba}>
                  <Button className={styles.btn_addtoarba}><span>أضف الي العربه</span></Button>
                  <Button className={styles.btn_addtoarba2}><span>أطلب الآن</span></Button>
              </div>
            </Grid>   

            <Grid item  xl={4} lg={4} md={4}  className={styles.grid} >
              <div className={styles.div_info_product}>
                  <Link href='#' className={styles.link_product}>سبراي تفحيم الكاوتش</Link> 

                  <Image  src={Img} alt="error"  width={85} height={85} className={styles.img_fav_product} />
                  <div >
                      {child2}
                      <IconButton sx={{margin:'0px',padding:'0px'}}>
                          <DeleteIcon sx={{fontSize:'28px',margin:'0px',padding:'0px',color:'#FF4966' }} />
                      </IconButton>
                  </div>
              </div>

              <div className={styles.div_info_product2}>
                  <p className={styles.money}>120 ريال سعودي </p>
                  <p className={styles.profit}>السعر  </p>
              </div>

              <div className={styles.div_info_product2}>
                  <p className={styles.money}>20 ريال سعودي </p>
                  <p className={styles.profit}>الربح  </p>
              </div>

              <div className={styles.addtoarba}>
                  <Button className={styles.btn_addtoarba}><span>أضف الي العربه</span></Button>
                  <Button className={styles.btn_addtoarba2}><span>أطلب الآن</span></Button>
              </div>
            </Grid>   
   

          </Grid>
        </Box>
    </Container>
    
      



   

  )
}
