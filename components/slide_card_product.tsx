import { Splide, SplideSlide } from '@splidejs/react-splide';
import '@splidejs/splide/dist/css/themes/splide-default.min.css';
import Image from 'next/image';
import Container from '@mui/material/Container';

import styles from '@/styles/slide_card_product.module.css'


interface SliderProps { 
    slides: {
      id: number;
      imageUrl: string;
      product_title: string;
      product_price: number;
      profit: number;
    }[];
    options?: {
      perPage?: number;
      perMove?: number;
      width?: number;
      height?: number;
    };
  }

function SlideCardProduct ({ slides, options }: SliderProps){


  const splideOptions = {
    type: 'loop',
    perPage: 1,
    perMove: 1,

    pagination: true,
    arrows: true,
  };

  return (
    
        <Splide options={splideOptions} className={styles.slide_card_product}>
          {slides.map((slides) => (
              <SplideSlide key={slides.id } className={styles.div_img_product}>
                <Image src={slides.imageUrl} alt={''} width={300} height={300} className={styles.img_slideshow}/>
              </SplideSlide>
          ))}
        </Splide>    
    
  );
};

export default SlideCardProduct;