import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import Button from '@mui/material/Button';
import Menu, { MenuProps } from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { useTheme } from '@mui/material/styles';
import Drawer from '@mui/material/Drawer';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import CssBaseline from '@mui/material/CssBaseline';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import useScrollTrigger from '@mui/material/useScrollTrigger';
import Slide from '@mui/material/Slide';
import Badge, { BadgeProps } from '@mui/material/Badge';

import Link from 'next/link';




// the_icons
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import PermIdentityIcon from '@mui/icons-material/PermIdentity';
import WalletIcon from '@mui/icons-material/Wallet';
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import LoginIcon from '@mui/icons-material/Login';
import DialpadIcon from '@mui/icons-material/Dialpad';
import DashboardIcon from '@mui/icons-material/Dashboard';
import AutoFixNormalIcon from '@mui/icons-material/AutoFixNormal';
import DynamicFormIcon from '@mui/icons-material/DynamicForm';
import ChaletIcon from '@mui/icons-material/Chalet';
import CleanHandsIcon from '@mui/icons-material/CleanHands';
import BackspaceIcon from '@mui/icons-material/Backspace';
import PersonIcon from '@mui/icons-material/Person';


// the_style
import styles from '@/styles/appbar.module.css'
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';


import { CartContext } from '../../src/CartContext';
import { FavContext } from '../../src/FavContext';
import { useContext } from 'react';
import axios from 'axios';


const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));


const StyledMenu = styled((props: MenuProps) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    {...props}
  />
))(({ theme }) => ({
  '& .MuiPaper-root': {
    borderRadius: 6,
    marginTop: theme.spacing(1),
    minWidth: 180,
    color:
      theme.palette.mode === 'light' ? 'rgb(55, 65, 81)' : theme.palette.grey[300],
    boxShadow:
      'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
    '& .MuiMenu-list': {
      padding: '4px 0',
    },
    '& .MuiMenuItem-root': {
      '& .MuiSvgIcon-root': {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      '&:active': {
        backgroundColor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity,
        ),
      },
    },
  },
}));


const drawerWidth = 240;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })<{
  open?: boolean;
}>(({ theme, open }) => ({
  flexGrow: 1,
  padding: theme.spacing(3),
  transition: theme.transitions.create('margin', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  marginRight: -drawerWidth,
  ...(open && {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginRight: 0,
  }),
}));

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginRight: drawerWidth,
  }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-start',
}));


interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
  children?: React.ReactNode;
}

function HideOnScroll(props: Props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
    
  });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      <AppBar>{children}</AppBar>
    </Slide>
  );
}


const StyledBadge = styled(Badge)<BadgeProps>(({ theme }) => ({
  '& .MuiBadge-badge': {
    backgroundColor: '#46B8B0',

    right: -3,
    top: 13,
    border: `2px solid ${theme.palette.background.paper}`,
    padding: '0 4px',
  },
}));










 function Appbar(props: Props) {
  const { cartItemsCount } = useContext(CartContext);
  const { FavItemsCount } = useContext(FavContext);

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  


  const theme = useTheme();
  const [open2, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <>
          <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar position="fixed" open={open2}>
              <Toolbar className={styles.toolbar}>
                <div className={styles.div_userinfo}>
                  <IconButton
                    size="large"
                    edge="start"
                    color="inherit"
                    aria-label="open drawer"
                    sx={{ mr: 2 }}
                  >

                  </IconButton>
                  <Typography variant="h6" noWrap sx={{ flexGrow: 1 }} component="div">


                    <Button
                      id="demo-customized-button"
                      aria-controls={open ? 'demo-customized-menu' : undefined}
                      aria-haspopup="true"
                      aria-expanded={open ? 'true' : undefined}
                      variant="contained"
                      disableElevation
                      onClick={handleClick}
                      className={styles.info_user}
                      startIcon={<ArrowBackIosIcon sx={{ color:'#202035',marginRight:'10px',fontSize:'48px',rotate:'-90deg' }}/>} >

                      <p className={styles.name_user}>أهلاَ محمد علي</p>   
                      <PermIdentityIcon sx={{ color:'#2b7b76',marginLeft:'10px',paddingBottom:'7px',fontSize:'38px' }}/>
                        
                    </Button>
                    <StyledMenu
                      id="demo-customized-menu"
                      MenuListProps={{
                        'aria-labelledby': 'demo-customized-button',
                      }}
                      anchorEl={anchorEl}
                      open={open}
                      onClose={handleClose} >
                    
                      <MenuItem onClick={handleClose} disableRipple className={styles.menuitem}>
                        <Link href='#' className={styles.name_account}>حسابي</Link>
                        <PermIdentityIcon className={styles.icon_account}/>
                      </MenuItem>

                      <MenuItem onClick={handleClose} disableRipple className={styles.menuitem}>
                        <Link href='/wallet' className={styles.name_account}>المحفظه</Link>
                        <Link href='/wallet'><WalletIcon className={styles.icon_account}/></Link>
                      </MenuItem>
                      
                      {/* <Divider sx={{ my: 0.5 }} /> */}
                      <MenuItem onClick={handleClose} disableRipple className={styles.menuitem}>
                        <Link href='#' className={styles.name_account}>الطلبات</Link>
                        <BookmarkBorderIcon className={styles.icon_account}/>
                      </MenuItem>

                      <MenuItem onClick={handleClose} disableRipple className={styles.menuitem}>
                        <Link href='#' className={styles.name_account}>تسجيل خروج</Link>
                        <LoginIcon className={styles.icon_account}/>
                      </MenuItem>

                    </StyledMenu>

                  </Typography>
                </div>


                <div className={styles.div_input}>
                  <Link href="/fav">
                    <IconButton aria-label="favourite" className={styles.icon_fav}>
                      <StyledBadge badgeContent={FavItemsCount} color="primary">
                        <FavoriteBorderIcon />
                      </StyledBadge>
                    </IconButton>
                  </Link> 

                  <Link href="#">
                    <IconButton aria-label="cart" className={styles.icon_fav}>
                      <StyledBadge badgeContent={cartItemsCount} color="primary">
                        <ShoppingCartIcon />
                      </StyledBadge>
                    </IconButton>
                  </Link>  

                  <form>
                    <Search className={styles.input_search}>
                      <SearchIconWrapper>
                        <SearchIcon className={styles.icon_search} />
                      </SearchIconWrapper>
                      <StyledInputBase 
                        className={styles.name_search}
                        // placeholder="ابحث عن المنتجات..."
                        inputProps={{ 'aria-label': 'search' }}
                        
                        />
                    </Search>
                  </form>

                  {/* <img src='https://taager.com/assets/img/header-icons/taager-logo-2022.svg' className={styles.logo} /> */}
                  <h2 className={styles.logo}>شركة الحبايب</h2>
                </div>

                  
                <IconButton
                  color="inherit"
                  aria-label="open drawer"
                  edge="end"
                  onClick={handleDrawerOpen}
                  sx={{ ...(open2 && { display: 'none' }) }}
                  className={styles.menu_icon}
                >
                  <DialpadIcon />
                </IconButton>

              </Toolbar>
            </AppBar>


            <Main open={open}>
              <DrawerHeader />
              <Typography paragraph>
                 
              </Typography>
              <Typography paragraph>
                 
              </Typography>
            </Main>
            <Drawer
              sx={{
                width: drawerWidth,
                flexShrink: 0,
                '& .MuiDrawer-paper': {
                  width: drawerWidth,
                },
              }}
              variant="persistent"
              anchor="right"
              open={open2}
            >
              <DrawerHeader className={styles.drawerheader}>
                <BackspaceIcon onClick={handleDrawerClose} className={styles.iconback}>
                  {theme.direction === 'rtl' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                </BackspaceIcon>
                <p className={styles.logoinphone}>شركة الحبايب</p>
              </DrawerHeader>

              <div className={styles.div_logoinphone}>
                <ArrowBackIosIcon className={styles.arrowbackiosicon}/>
                <div className={styles.div_logoinphone2}>
                  <Link href='/' className={styles.link_product}>الرئيسيه</Link>
                  <AutoFixNormalIcon className={styles.icon_product}/>
                </div>
              </div>
              <div className={styles.div_logoinphone}>
                <ArrowBackIosIcon className={styles.arrowbackiosicon}/>
                <div className={styles.div_logoinphone2}>
                        <Link href='#' className={styles.name_account2}>حسابي</Link>
                        <PermIdentityIcon className={styles.icon_account2}/>
                </div>
              </div>
              <div className={styles.div_logoinphone}>
                <ArrowBackIosIcon className={styles.arrowbackiosicon}/>
                <div className={styles.div_logoinphone2}>
                  <Link href='/wallet' className={styles.name_account2}>المحفظه</Link>
                  <WalletIcon className={styles.icon_account2}/>
                </div>
              </div>
              <div className={styles.div_logoinphone}>
                <ArrowBackIosIcon className={styles.arrowbackiosicon}/>
                <div className={styles.div_logoinphone2}>
                  <Link href='#' className={styles.name_account2}>الطلبات</Link>
                  <BookmarkBorderIcon className={styles.icon_account2}/>
                </div>
              </div>

              <div className={styles.div_logoinphone3}>
                <Link href='#' className={styles.name_account3}>تسجيل خروج</Link>
                <LoginIcon className={styles.icon_account3}/>
              </div>





            </Drawer>
          </Box>

          <React.Fragment >
            <CssBaseline />
            <HideOnScroll {...props}>
              <AppBar className={styles.appbar_products}>
                <Toolbar>
                  <div className={styles.typography_products}>
                    <div className={styles.div_name_products}>


                      <div className={styles.name_products}>
                        <div className={styles.div_product}>
                          <Link href='#' className={styles.link_product}>الصحه والجمال</Link>
                          <CleanHandsIcon className={styles.icon_product}/>
                        </div>

                        <div className={styles.div_product}>
                          <Link href='#' className={styles.link_product}>المنزل</Link>
                          <ChaletIcon className={styles.icon_product}/>
                        </div>

                        <div className={styles.div_product}>
                          <Link href='#' className={styles.link_product}>إلكترونيات</Link>
                          <DynamicFormIcon className={styles.icon_product}/>
                        </div>

                        <div className={styles.div_product}>
                          <Link href='/' className={styles.link_product}>الرئيسيه</Link>
                          <AutoFixNormalIcon className={styles.icon_product}/>
                        </div>

                        <div className={styles.total_products}>
                          <Link href='#' className={styles.total_name_products}>جميع الفئات</Link>
                          <DashboardIcon className={styles.total_icon_products}/>
                        </div>
                        
                      </div>
                    </div>
            
                  </div>
                </Toolbar>
              </AppBar>
            </HideOnScroll>
            <Toolbar />
           
          </React.Fragment>

          


  </>        
  );
}

export default Appbar;