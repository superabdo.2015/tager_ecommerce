import { Splide, SplideSlide } from '@splidejs/react-splide';
import '@splidejs/splide/dist/css/themes/splide-default.min.css';
import Image from 'next/image';
import Container from '@mui/material/Container';
import { useMediaQuery } from 'react-responsive';
import Card_product from '../components/card_product';

import styles from '@/styles/slider.module.css'


interface SliderProps { 
    slides: {
      id: number;
      imageUrl: string;
      product_title: string;
      product_price: number;
      profit: number;
    }[];
    options?: {
      perPage?: number;
      perMove?: number;
      width?: number;
      height?: number;
    };
  }

function Slider ({ slides, options }: SliderProps){
    const isDesktop = useMediaQuery({
        query: "(min-width: 768px)",
        });


  const splideOptions = {
    type: 'loop',
    perPage: isDesktop ? 4 : 2,
    perMove: options?.perMove || 2,
    speed: 1300,
    pagination: false,
    arrows: true,

  };

  return (
      <Container>
        <Splide options={splideOptions} className={styles.splide}>
          {slides.map((slide) => (
              <SplideSlide key={slide.id } className={styles.div_img_product}>
                
               <Card_product  imageulr={slide.imageUrl} className={styles.card_img_product} p_id={slide.id}/>

              </SplideSlide>
          ))}
        </Splide> 
      </Container>
   
  );
};

export default Slider;