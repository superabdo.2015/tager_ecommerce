
import React from 'react'
import Container from '@mui/material/Container';
import Divider from '@mui/material/Divider';

import styles from '@/styles/catagory_index.module.css'
import Link from 'next/link';





function CatagoryIndex( props:any ) {
  return (
    <Container>
        <div className={styles.catagory_index}>
            <Link href={props.url} className={styles.link_showtotal}>عرض الكل</Link>
            <p  className={styles.name_showtotal}>{props.title}</p>
        </div>
    </Container>

  )
}

export default CatagoryIndex;