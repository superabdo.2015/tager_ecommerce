import styles from '@/styles/card_product.module.css'
import Image from 'next/image';
import { useSnackbar } from 'notistack'
import { Button, colors } from '@mui/material';
import FavoriteBorder from '@mui/icons-material/FavoriteBorder';
import Favorite from '@mui/icons-material/Favorite';

// the icons
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import { CartContext } from '../src/CartContext';
import { FavContext } from '../src/FavContext';
import { useCallback, useContext, useEffect, useState } from "react";
import axios from 'axios';
import Link from 'next/link';




function CardProduct(props: any): JSX.Element {
  const apiUrl = "http://localhost:5000/users/123";
  const { addToCart } = useContext(CartContext);
  const { addToFav, removeFromFav } = useContext(FavContext);
  const { enqueueSnackbar } = useSnackbar();

  const [isFavourite, setIsFavourite] = useState(false);

  const checkFavouriteItem = useCallback(async () => {
    try {
      const response = await axios.get(
        `${apiUrl}/checkFavouriteItem/${props.p_id}`
      );
      setIsFavourite(response.data === "found");
    } catch (error) {
      console.error(error);
    }
  }, [props.p_id]);

  useEffect(() => {
    checkFavouriteItem();
  }, [checkFavouriteItem]);

  const handleAddToFavourite = useCallback(async () => {
    try {
      const endpoint = isFavourite ? "removeFromFavourite" : "addToFavourite";
      await axios.post(`${apiUrl}/${endpoint}/${props.p_id}`);
      if (isFavourite) {
        removeFromFav();
        enqueueSnackbar("تم إزالة المنتج من المفضله", {
          autoHideDuration: 3000,
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
          variant: "error",
        });
      } else {
        addToFav();
        enqueueSnackbar("تم إضافة المنتج الي المفضله", {
          autoHideDuration: 3000,
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
          variant: "success",
        });
      }
      setIsFavourite(!isFavourite);
    } catch (error) {
      console.error(error);
      enqueueSnackbar("حدث خطأ ما، يرجى المحاولة مرة أخرى", {
        autoHideDuration: 3000,
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
        variant: "error",
      });
    }
  }, [isFavourite, props.p_id, enqueueSnackbar, addToFav, removeFromFav]);

  const handleAddToCart = useCallback(() => {
    addToCart();
    enqueueSnackbar("تم إضافة المنتج الي العربه", {
      autoHideDuration: 1000,
      anchorOrigin: {
        vertical: "top",
        horizontal: "right",
      },
      variant: "success",
    });
  }, [addToCart, enqueueSnackbar]);


  return (
    
  <div className={styles.card}>

    <div >
      {/* <Image src='/img/bg3.jpeg' alt={''} className={styles.img} width={0} height={0}/> */}
      <Image src={props.imageulr} alt={''} width={200} height={180} className={styles.img} quality={100} loading="lazy"/>
    </div>
    
    <div className={styles.div_name_product}>
        <Link href="#" className={styles.name_product}>موزع الحبوب الدوار من 6 أقسام</Link>
    </div>

    <div className={styles.info_products}>
      <div className={styles.profit_user}>
        <p className={styles.low_profit}>اقل ربح لك</p>
        <p  className={styles.low_profit2}>20 ر.س</p>
      </div>

      <div className={styles.profit_user2} >
        <p className={styles.low_profit}> سعر البيع</p>
        <div className={styles.price_product}>
          <del className={styles.price_product3}>120</del>
          <p className={styles.price_product4}>110 ر.س</p>
        </div>
      </div>
    </div>

    <div className={styles.div_buttons_fav_profit}>
      <Button onClick={handleAddToFavourite} >
        {isFavourite ? <Favorite className={styles.icon_fav1} />: <FavoriteBorder className={styles.icon_fav2}/>}
      </Button>


      <Button onClick={handleAddToCart} className={styles.div_addtoarba}>
        <AddShoppingCartIcon sx={{ color:'#5C5C5D' }} className={styles.addShoppingCart}/>
        <p className={styles.addtoarba}>إضافة الي العربة</p>
      </Button>
    </div>

    <div>
      
    </div>
  </div>
  )
}

export default CardProduct;
